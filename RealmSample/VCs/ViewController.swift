//
//  ViewController.swift
//  RealmSample
//
//  Created by javad faghih on 1/29/1400 AP.
//

import UIKit
import RealmSwift


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    var card: Results<myCards>!
    
    @IBOutlet weak var tableView: UITableView!

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (row, indexpath) in
           
            
            self.alertForEdit(cards: self.card[indexpath.row])
            
            
            tableView.reloadData()
            
        }
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexpath) in
            
            RealmManager.shared.deleteDataInRealm(self.card[indexPath.row])
            tableView.reloadData()
            
        }
        
        return [edit, delete]
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return card?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "myCards", for: indexPath) as? MyCardCell else { return UITableViewCell()}
        
   //     guard let cards = card else { return  }
        
        cell.configCell(cards: card[indexPath.row] )
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        card = RealmManager.shared.readMyCardsFromDB()
        
    }
    
    @IBAction func addButtonWasPressed(_ sender: Any) {
        
        alert()
    
    }
    
    @IBAction func infoButtonWasPressed(_ sender: UIBarButtonItem) {
        
        RealmManager.shared.deleteAllDataFromRealm()
        tableView.reloadData()
    }
    
    @IBAction func destCardWasPressed(_ sender: Any) {
  
    performSegue(withIdentifier: "destCard", sender: self)
    
    }
    
    @IBAction func userInfoButtonWasPressed(_ sender: Any) {
        
        
        performSegue(withIdentifier: "userInfo", sender: self)

    }
    
    
    
    private func alert() {
        
        let alert = UIAlertController(title: "add Cards", message: "please fill all fields", preferredStyle: .alert)
        
        var cardNumber = UITextField()

        var month = UITextField()

        var year = UITextField()

        var bankImageName = UITextField()

        var isDefaults = UITextField()
        
        alert.addTextField { (textField) in
            textField.placeholder = "Card Number"
            
            cardNumber = textField
        }
        
        alert.addTextField { (textFiled) in
            textFiled.placeholder = "Month"
            month = textFiled
        }
        
        alert.addTextField { (textField) in
            textField.placeholder = "year"
            
            year = textField
        }
        
        alert.addTextField { (textFiled) in
            textFiled.placeholder = "bankImageName"
            bankImageName = textFiled
        }
        
        alert.addTextField { (textFiled) in
            textFiled.placeholder = "is defaults"
            isDefaults = textFiled
        }
        
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            
            var cards = myCards()
            cards.bankimageName = bankImageName.text!
            cards.cardNumber = cardNumber.text!
            cards.month = month.text!
            cards.isDefault = isDefaults.text == "1" ? true : false
            cards.year = year.text!
            
            RealmManager.shared.addDataToRealm(object: cards)
            self.tableView.reloadData()
        }
        
        let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        present(alert, animated: true) {
            
        }
        
    }
    
    private func alertForEdit(cards: myCards) {
        
        let alert = UIAlertController(title: "add Cards", message: "please fill all fields", preferredStyle: .alert)
        
        var cardNumber = UITextField()

        var month = UITextField()

        var year = UITextField()

        var bankImageName = UITextField()

        var isDefaults = UITextField()
        
        alert.addTextField { (textField) in
            textField.placeholder = "Card Number"
            textField.text = cards.cardNumber
            cardNumber = textField
        }
        
        alert.addTextField { (textFiled) in
            textFiled.text = cards.month
            textFiled.placeholder = "Month"
            month = textFiled
        }
        
        alert.addTextField { (textField) in
            textField.placeholder = "year"
            textField.text = cards.year
            year = textField
        }
        
        alert.addTextField { (textFiled) in
            textFiled.placeholder = "bankImageName"
            textFiled.text = cards.bankimageName
            bankImageName = textFiled
        }
        
        alert.addTextField { (textFiled) in
            textFiled.placeholder = "is defaults"
            textFiled.text = cards.isDefault ? "true" : "false"
            isDefaults = textFiled
        }
        
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            
            
            
            RealmManager.shared.updateMyCardsInRealm(cards, cardNumber: cardNumber.text!, month: month.text!, year: year.text!, bankImageName: bankImageName.text!, isDefault: isDefaults.text == "1" ? true : false)
            self.tableView.reloadData()
        }
        
        let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        present(alert, animated: true) {
            
        }
        
    }
    
    

    
    
}

