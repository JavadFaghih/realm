//
//  DataModelsManager.swift
//  Onboarding
//
//  Created by javad faghih on 12/29/1399 AP.
//  Copyright © 1399 Training. All rights reserved.
//

import Foundation
import RealmSwift

final class RealmManager {
    
    static let shared = DataModelManger()
    
    private var realm: Realm
    
    private init() {
        realm = try! Realm()
    }
    
    func addDataToRealm<T: Object>(object: T) {
        
        self.deleteAllDataFromRealm()
        do {
            try realm.write {
                
                realm.add(object)
                print("Added new datamodel to realm")
            }
        }
        catch {
            print("error saving to Realm...")
            
        }
    }
    
    func updateDataInRealm<T: Object>(_ object: T) {
        self.deleteAllDataFromRealm()
        do {
            try realm.write {
                realm.add(object, update: .modified)
                
                print("object modified in realm DB")
            }
        }
        catch {
            print("error upadating to Realm...")
        }
    }
    
    
    func readDataFromDB() -> Results<DataModel> {
        let results: Results<DataModel> =   realm.objects(DataModel.self)
        return results
    }
    
   private func deleteAllDataFromRealm() {
        do {
            try realm.write {
                realm.deleteAll()
                
            }
        }
        catch {
            print("error deleting from realm...")
            
        }
    }
    
    func deleteDataInRealm<T: Object>(_ object: T) {
        
        do {
            try realm.write {
                realm.delete(object)
            }
        }
        catch {
            print("error deleting from realm...")

        }
        
    }
    
}

