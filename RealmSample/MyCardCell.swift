//
//  MyCardCell.swift
//  RealmSample
//
//  Created by javad faghih on 1/29/1400 AP.
//

import UIKit
import RealmSwift

class MyCardCell: UITableViewCell {

    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var month: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var bankImageName: UILabel!
    @IBOutlet weak var isDefaults: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(cards: myCards) {
        cardNumber.text = cards.cardNumber
        month.text = cards.month
        year.text = cards.year
        bankImageName.text = cards.bankimageName
        isDefaults.text = cards.isDefault ? "true" : "false"
        
    }
    
    
}
