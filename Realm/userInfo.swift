//
//  userInfo.swift
//  Sayanapp
//
//  Created by APPLE on 4/30/1399 AP.
//  Copyright © 1399 technotapp. All rights reserved.
//

import Foundation
import RealmSwift


class UserInfo: Object {


    @objc dynamic var hasProfile = false
    @objc dynamic var cdnUrl = ""
    @objc dynamic var Avatar = ""
    @objc dynamic var FirstName = ""
    @objc dynamic var IsActive = true
    @objc dynamic var LastName = ""
    @objc dynamic var TokenId = ""
    @objc dynamic var MobileNumber = ""
    @objc dynamic var cityId = 1
    @objc dynamic var provinceId = 1
    @objc dynamic var address = ""
    @objc dynamic var email = ""
    @objc dynamic var nationalCode = ""
    @objc dynamic var genderId = 1
    @objc dynamic var dateOfBirth = Date().GetTimeStamp()
    @objc dynamic var avatarImageFile = ""
    
    

}
