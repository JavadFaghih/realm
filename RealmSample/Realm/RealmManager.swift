//
//  DataModelsManager.swift
//  Onboarding
//
//  Created by javad faghih on 12/29/1399 AP.
//  Copyright © 1399 Training. All rights reserved.
//

import Foundation
import RealmSwift

final class RealmManager {
    
    static let shared = RealmManager()
 
    private var realm: Realm
    private var config = Realm.Configuration(encryptionKey: getKey())
    
    private init() {
        realm = try! Realm(configuration: config)
    }
    
    
    func addDataToRealm<T: Object>(object: T) {
        
        do {
            try realm.write {
                realm.add(object)
                print("Added new datamodel to realm")
            }
        }
        catch {
            print("error saving to Realm...")
        }
    }

    func updateMyCardsInRealm(_ object: myCards, cardNumber: String, month: String, year: String, bankImageName: String, isDefault: Bool ) {
        
        do {
            try realm.write {
                object.bankimageName = bankImageName
                object.cardNumber = cardNumber
                object.month = month
                object.year = year
                object.isDefault = isDefault
                print("object modified in realm DB")
            }
        }
        catch {
            print("error upadating to Realm...")
        }
    }
    
    func readMyCardsFromDB() -> Results<myCards> {
        let results: Results<myCards> =   realm.objects(myCards.self)
        return results
    }
    
    func deleteAllDataFromRealm() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        }
        catch {
            print("error deleting from realm...")
            
        }
    }
    
    func deleteDataInRealm<T: Object>(_ object: T) {
        
        do {
            try realm.write {
                realm.delete(object)
            }
        }
        catch {
            print("error deleting from realm...")
            
        }
    }
}

